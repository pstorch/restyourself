package de.storc.restyourself;

import com.jcabi.http.request.JdkRequest;
import com.jcabi.http.response.RestResponse;
import com.jcabi.http.response.XmlResponse;
import com.jcabi.http.wire.VerboseWire;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.HttpHeaders;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.Files;

public class RESTYourselfServerTest {

    protected static final String HOME = "http://localhost:8080";
    public static final String AUTH = "Basic UkVTVDpZb3Vyc2VsZg==";
    public static final String XML = "application/xml";
    public static final String JSON = "application/json";

    private static RESTYourselfServer server;

    @BeforeClass
    public static void setUp() {
        server = new RESTYourselfServer();
        server.start();
    }

    @AfterClass
    public static void tearDown() {
        server.stop();
    }

    @Test
    public void testTask1Wrong() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching").back()
                .through(VerboseWire.class)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_NOT_ACCEPTABLE);
    }

    @Test
    public void testTask1Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_OK);
    }

    @Test
    public void testTask2Wrong() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/pocketqueries").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_UNAUTHORIZED);
    }

    @Test
    public void testTask2Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/pocketqueries").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_OK);
    }

    @Test
    public void testPocketQueriesXsd() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/pocketqueries.xsd").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .fetch()
                .as(XmlResponse.class)
                .registerNs("xsd", "http://www.w3.org/2001/XMLSchema")
                .assertXPath("/xsd:schema/xsd:element[@name='pocketquery']");
    }

    @Test
    public void testTask3WrongType() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/pocketqueries").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .method(HttpMethod.POST)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_UNSUPPORTED_TYPE);
    }

    @Test
    public void testTask3WrongRequest() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/pocketqueries").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.CONTENT_TYPE, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .method(HttpMethod.POST)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_BAD_REQUEST);
    }

    @Test
    public void testTask3Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/pocketqueries").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.CONTENT_TYPE, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .method(HttpMethod.POST)
                .body().set(getBytesFromResource("examplepq.xml")).back()
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_CREATED);
    }

    private byte[] getBytesFromResource(final String resource) throws IOException {
        return Files.readAllBytes(new File(getClass().getClassLoader().getResource(resource).getPath()).toPath());
    }

    @Test
    public void testTask4Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/pocketqueries/123456789").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .fetch()
                .as(XmlResponse.class)
                .assertXPath("/pocketqueryresult/geocache[@id='GC2MZPW']");
    }

    @Test
    public void testTask5Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/geocaches/GC2MZPW").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .fetch()
                .as(XmlResponse.class)
                .assertXPath("/geocache[@id='GC2MZPW']");
    }

    @Test
    public void testTask6WrongParameters() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/geocaches/GC2MZPW/logs").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_BAD_REQUEST);
    }

    @Test
    public void testTask6Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/geocaches/GC2MZPW/logs")
                .queryParam("orderBy", "DateDescending")
                .queryParam("maxLogs", "10")
                .back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .fetch()
                .as(XmlResponse.class)
                .assertXPath("/logs[@found='250']/log[@user='GeOcAcHeR']");
    }

    @Test
    public void testTask7Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/geocaches/GC2MZPW/logs")
                .queryParam("orderBy", "DateDescending")
                .queryParam("maxLogs", "10")
                .back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .header(HttpHeaders.IF_NONE_MATCH, "686897696a7c876b7e")
                .fetch()
                .as(XmlResponse.class)
                .assertXPath("/logs[@found='251']/log[@user='MegaCacher']");
    }

    @Test
    public void testTask8WrongType() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/geocaches/GC2MZPW/logs").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .method(HttpMethod.POST)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_UNSUPPORTED_TYPE);
    }

    @Test
    public void testTask8WrongContent() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/geocaches/GC2MZPW/logs").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.CONTENT_TYPE, JSON)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .body().set("{ \"key\": \"value\" }").back()
                .method(HttpMethod.POST)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_BAD_REQUEST);
    }

    private void assertTask8Success(final String logResource) throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/geocaches/GC2MZPW/logs").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.CONTENT_TYPE, JSON)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .body().set(getBytesFromResource(logResource)).back()
                .method(HttpMethod.POST)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_CREATED);
    }

    @Test
    public void testTask8Success() throws IOException {
        assertTask8Success("examplelog.json");
    }

    @Test
    public void testTask8Success2() throws IOException {
        assertTask8Success("examplelog2.json");
    }

    @Test
    public void testTask8Success3() throws IOException {
        assertTask8Success("examplelog3.json");
    }

    @Test
    public void testTask9Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/geocaches/GC2MZPW/logs/4711").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.CONTENT_TYPE, JSON)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .body().set(getBytesFromResource("logupdate.json")).back()
                .method(HttpMethod.PUT)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_CREATED);
    }

    @Test
    public void testTask10Success() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/pocketqueries/123456789").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .method(HttpMethod.DELETE)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_GONE);
    }

    @Test
    public void testProfiles() throws IOException {
        new JdkRequest(HOME).uri().path("/exam/geocaching/profiles").back()
                .through(VerboseWire.class)
                .header(HttpHeaders.ACCEPT, XML)
                .header(HttpHeaders.AUTHORIZATION, AUTH)
                .fetch()
                .as(RestResponse.class)
                .assertStatus(HttpURLConnection.HTTP_OK);
    }

}
