package de.storc.restyourself;

import javax.servlet.http.HttpServlet;

public class Resource {

    private HttpServlet servlet;

    private boolean memberArea = false;

    public Resource(HttpServlet servlet, boolean memberArea) {
        super();
        this.servlet = servlet;
        this.memberArea = memberArea;
    }

    public HttpServlet getServlet() {
        return servlet;
    }

    public boolean isMemberArea() {
        return memberArea;
    }

}
