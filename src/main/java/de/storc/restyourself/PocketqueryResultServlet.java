package de.storc.restyourself;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PocketqueryResultServlet extends AbstractExamServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        assertAcceptXML(req);
        sendXML(resp, "pocketqueryresult.xml");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setStatus(HttpServletResponse.SC_GONE);
        sendXML(resp, "pocketquerydeleted.xml");
    }

}
