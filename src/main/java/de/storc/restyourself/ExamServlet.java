package de.storc.restyourself;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ExamServlet extends AbstractExamServlet {

    private static final long serialVersionUID = 1L;

    private static Map<String, Resource> resources = new HashMap<>();

    static {
        resources.put("/geocaching", new Resource(new GeocachingServlet(),
                false));
        resources.put("/geocaching/geocaches", new Resource(
                new GeocachesServlet(), false));
        resources.put("/geocaching/profiles", new Resource(
                new ProfilesServlet(), true));
        resources.put("/geocaching/pocketqueries", new Resource(
                new PocketqueriesServlet(), true));
        resources.put("/geocaching/pocketqueries.xsd", new Resource(
                new PocketqueriesXsdServlet(), false));
        resources.put("/geocaching/pocketqueries/123456789", new Resource(
                new PocketqueryResultServlet(), true));
        resources.put("/geocaching/geocaches/GC2MZPW", new Resource(
                new GeocacheServlet(), true));
        resources.put("/geocaching/geocaches/GC2MZPW/logs", new Resource(
                new GeocacheLogsServlet(), true));
        resources.put("/geocaching/geocaches/GC2MZPW/logs/4711", new Resource(
                new GeocacheLogServlet(), true));
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        log.info(req.getMethod() + " " + req.getPathInfo());
        try {
            Resource resource = resources.get(req.getPathInfo());
            if (resource == null) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }

            if (resource.isMemberArea()) {
                String authorization = req.getHeader("authorization");
                log.info("authorization=" + authorization);
                if (!"Basic UkVTVDpZb3Vyc2VsZg==".equals(authorization)) {
                    resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    resp.setHeader("WWW-Authenticate",
                            "Basic realm=\"Member Area\"");
                    return;
                }
            }
            resource.getServlet().service(req, resp);
        } catch (NotAcceptableException e) {
            resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            resp.setContentType(e.getExpectedContentType());
        } catch (UnsupportedMediaTypeException e) {
            resp.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
        }
    }

}
