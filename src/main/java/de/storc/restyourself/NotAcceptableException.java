package de.storc.restyourself;

public class NotAcceptableException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String expectedContentType;

    public NotAcceptableException(String expectedContentType) {
        super();
        this.expectedContentType = expectedContentType;
    }

    public String getExpectedContentType() {
        return expectedContentType;
    }

}
